const defaultPort = 3017;

const config = {
    defaultPort,
    publicServer: `http://127.0.0.1:${defaultPort}`,

    postgres: {
        host    : 'phonebook_db_1',
        port    : 3018,
        database: 'phonebook',
        user    : 'phonebook',
        password: 'phonebook'
    },

    logs: {},

    secrets: {
        GitHub: {
            CLIENT_ID: 'e95df0f7fbab1b5a1ead',
            CLIENT_SECRET: 'bda64060630df16361b236e173dd3c0ab2cd4fc5'
        }
    },

    expireDates: {
        mainToken      : { value: 7, unit: 'd' },
        retrospective  : { value: 1, unit: 'h' },
        iterationRating: { value: 1, unit: 'h' }
    }
};

module.exports = config;
