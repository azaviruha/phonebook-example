
const errors = {
  INTERNAL       : { code: 'ERRINTERNAL', message: 'Internal server error' },
  INVALID_PAYLOD : { code: 'ERRINVALIDPAYLOAD', message: 'Payload is not valid' },

  phone: {
    NOT_FOUND : { code: 'ERRPHONENOTFOUND', message: 'Phone is not found' },
  }
}

module.exports = errors
