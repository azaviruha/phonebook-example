'use strict'

const express = require('express')
const accountsAPI = express.Router()

const validators = require('../utils/validation')
const errors = require('../errors')

function createAccountsAPI (app, db) {
    const logger = app.get('logger')
    const config = app.get('config')

    // Account validation logic

    return accountsAPI
}

module.exports = createAccountsAPI