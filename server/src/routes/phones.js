'use strict'

const express  = require('express')
const phonesAPI = express.Router({ mergeParams: true })

const validators = require('../../utils/validation')
const serializers = require('../../utils/serializers')
const errors     = require('../../errors')


function createPhonesAPI(app, db) {
  const config    = app.get('config')
  const Phone     = require('../../models/Phone')(db, config)
  const logger    = app.get('logger')


  phonesAPI.get('/:phoneNumber', (req, resp) => {
    const { phoneNumber } = req.params

    Phone
      .find.byNumber(phoneNumber)
      .then(phone => {
        if (!phone) {
          resp.status(404)
          resp.json(errors.phone.NOT_FOUND)
        } else {
          resp.status(200)
          resp.json(serializers.Phone(phone))
        }
      })
      .catch(error => {
        logger.info(error)
        resp.status(500)
        resp.json(errors.INTERNAL)
      })
    })

    // ======================================================= //
    // ==================== SECURED URLS ===================== //
    // ======================================================= //

    phonesAPI.use(['/:phoneId', '/:phoneId/*'], (req, resp, next) => {
      // access checking logic
      next()
    })

    phonesAPI.post('/', (req, resp) => {
      const { error, value }   = validators.phones.newPhone.check(req.body)

      if (error) {
        logger.info(error)
        resp.status(400)
        resp.json(errors.INVALID_PAYLOD)
        return
      }

      Phone
        .create(value)
        .then(({ id }) => { resp.status(201).json({ id }) })
        .catch(error => {
          logger.info(error)
          resp.json(error)
        })
    })

    return phonesAPI
}

module.exports = createPhonesAPI