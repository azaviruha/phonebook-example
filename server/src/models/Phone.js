'use strict'

const util   = require('util')
const pgp    = require('pg-promise')(/*options*/)
const errors = require('../errors')
const sql    = require('../lib/sqlTools')

const { queryFile, handlePGPErrors, isNotFound } = sql

/**
 * SQL scripts
 */

function createPhonesModel(db, config) {
  function findPhoneByNumber(number) {
    const sqlFindPhoneByNumber = queryFile('find-phones-by-number.sql')

    return db.manyOrNone(sqlFindSprintTasks, { number })
  }

  return {
    // create: ...
    // remove: ...
    find: {
        byNumber: findPhoneByNumber
    },
    findAll: { 
      //...
    }
  }
}


module.exports = createPhonesModel
