const join         = require('path').join;
const fs           = require('fs');
const pgp          = require('pg-promise')(/*options*/);
const Mustache     = require('mustache');
const parsePgArray = require('postgres-array').parse;


function queryFile(filePath, config={}) {
    const path = join(__dirname, '../../sql/', filePath);

    return Mustache.render(fs.readFileSync(path, { encoding: 'utf8' }), config);
}


function handlePGPErrors(error) {
    switch (error.code) {
        case pgp.errors.queryResultErrorCode.noData:
            return Promise.resolve(null);
        default:
            return Promise.reject(error);
    }
}

function isNotFound(error) {
    return error && (error.code === pgp.errors.queryResultErrorCode.noData);
}


module.exports = {
    queryFile,
    handlePGPErrors,
    isNotFound
};