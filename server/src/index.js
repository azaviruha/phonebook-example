'use strict';

const fs = require('fs');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const expressWinston = require('express-winston');
const errors = require('./errors');

const accountsRouter = require('./routes/accounts');
const phonesRouter = require('./routes/phones');

function createApp(config, db) {
   if (!fs.existsSync('../logs')) {
        fs.mkdirSync('../logs');
    }

    const logs = require('./utils/logs')(config.logs);

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(bodyParser.text({ type: 'text/plain' }));

    app.set('config', config);
    app.set('secrets', config.secrets);
    app.set('publicServer', config.publicServer);
    app.set('logger', logs.loggers.normal);

    /**
     * Normal logger (register before routers).
     */

    app.use(expressWinston.logger(logs.configs.normal));

    app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

        setTimeout(() => {
            if ('OPTIONS' === req.method) {
                res.send(200);
            } else {
                next();
            }
        }, 2000);
    });

    app.use('/accounts', accountRouter(app, db));
    app.use('/phones', phonesRouter(app, db));

    /**
     * Error logger (register after routers).
     */
    app.use(expressWinston.errorLogger(logs.configs.error));

    app.use(function (err, req, resp, next) {
        // logger.error(err);
        resp.status(500);
        resp.json(errors.INTERNAL);
    });

    return app;
}

module.exports = createApp;
