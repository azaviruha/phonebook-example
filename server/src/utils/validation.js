const Joi = require('joi')

const newPhoneData = Joi.object().keys({
    name  : Joi.string().required(),
    number: Joi.string().required()
})

const rules = {
  pnones: {
    newPhone: newPhoneData
  }
}

function buildRuleCheckers(rules) {
    return Object.keys(rules).reduce((acc, ruleId) => {
        const maybeRule = rules[ruleId]

        if (maybeRule.isJoi) {
            acc[ruleId] = Object.assign(Object.create(maybeRule), { check, sanitize })
        } else {
            acc[ruleId] = buildRuleCheckers(maybeRule)
        }

        return acc
    }, {})
}

/**
 * Method `.check()` for rules.
 * `this` will be ref to rule.
 */
function check(data) {
    return Joi.validate(data, this, { presence: 'required', abortEarly: false })
}

function sanitize(data) {
    return Joi.attempt(data, this, { presence: 'required', abortEarly: false })
}

module.exports = buildRuleCheckers(rules)
