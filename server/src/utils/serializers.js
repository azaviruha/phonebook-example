const Joi = require('joi')

const phonesResult = Joi.object()
  .rename('phone_id', 'id')
  .rename('phone_number', 'number')
  .rename('abonent_fio', 'name')
  .keys({
    id    : Joi.string().guid({ version: ['uuidv4'] }).required(),
    userId: Joi.string().guid({ version: ['uuidv4'] }).required().strip(),
    number: Joi.string().required(),
    name  : Joi.string().required()
  })
  .required()

const rules = {
  Phone : phonesResult
}

function buildSerializers(rules) {
  return Object.keys(rules).reduce((acc, ruleId) => {
      const maybeRule = rules[ruleId]

      if (maybeRule.isJoi) {
          acc[ruleId] = data => Joi.attempt(data, maybeRule)
      } else {
          acc[ruleId] = buildSerializers(maybeRule)
      }

      return acc
  }, {})
}

module.exports = buildSerializers(rules)
