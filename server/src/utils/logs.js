'use strict';

const winston = require('winston');
const join    = require('path').join;
const path    = join(__dirname, '../../logs/');
const fs      = require('fs');

if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
}

winston.emitErrs = true;

function loggerConfigs(config) {
    const normalConfig = createNormalLoggerConfig(config);
    const errorConfig  = createErrorLoggerConfig(config);

    return {
        configs: {
            normal: normalConfig,
            error : errorConfig
        },
        loggers: {
            normal: new winston.Logger(normalConfig),
            error : new winston.Logger(errorConfig)
        }
    };
}


function createNormalLoggerConfig(config) {
    return {
        transports: [
            new winston.transports.File({
                level: 'info',
                filename: join(path, 'all-logs.log.json'),
                handleExceptions: true,
                json: true,
                maxsize: 5242880, //5MB
                maxFiles: 5,
                colorize: false
            })
        ].concat(config.hideConsole
            ? []
            : [new winston.transports.Console({
                level: 'debug',
                handleExceptions: true,
                json: false,
                colorize: true
            })]),

        exitOnError: false
    };
}


function createErrorLoggerConfig(config) {
    return {
        transports: [
            new winston.transports.File({
                level: 'error',
                filename: join(path, 'errors.log.json'),
                handleExceptions: true,
                json: true,
                maxsize: 5242880, //5MB
                maxFiles: 5,
                colorize: false
            })
        ],
        exitOnError: false,
        dumpExceptions: true,
        showStack: true
    };
}


module.exports = loggerConfigs;
