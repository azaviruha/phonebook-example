/**
 * Use it to seed db with fake data, for tests and development.
 */

const config = require('../config/dev');
const pgp    = require('pg-promise')(/*options*/);
const db     = pgp(config.postgres);

console.log('Starting DB cleaning...');

db
    .none('DELETE FROM users CASCADE;') 
    .then(_ =>  db.none('DELETE FROM phones CASCADE;'))
    .then(_ => { 
        console.log('DB successfully cleaned.');
        process.exit(0);
    })
    .catch(error => {
        console.log('error: ', error);
        process.exit(-1);
    });