'use strict';

const http   = require('http');
const config = require(`../config/${process.env.NODE_ENV}`);
const pgp    = require('pg-promise')(/*options*/);
const db     = pgp(config.postgres);
const app    = require('../src')(config, db);
const PORT   = process.env.PORT || config.defaultPort;
const logger = require('../src/utils/logs')(config.logs).loggers.normal;

http.createServer(app).listen(PORT);
logger.info('App starts on port ', PORT);
