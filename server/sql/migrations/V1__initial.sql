CREATE SCHEMA IF NOT EXISTS public;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users (
    user_id      uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    email        varchar(50) UNIQUE NOT NULL,
    password     varchar(20) NOT NULL,
    role         jsonb NOT NULL
);

CREATE TABLE phones (
    phone_id       uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id        uuid REFERENCES users ON DELETE CASCADE,
    phone_number   varchar(50) UNIQUE NOT NULL,
    abonent_fio    varchar(50) NOT NULL
);
