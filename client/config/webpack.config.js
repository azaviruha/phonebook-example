const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const resolve = dir => path.join(__dirname, '..', dir)

module.exports = {
  entry: {
    app: './src/index.js'
  },

  output: {
    path: resolve('out'),
    filename: '[name].bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [ resolve('src') ],
        loader: 'babel-loader',

        options: {
          presets: [ 'env', 'stage-3', 'react' ],
          plugins: [ 'react-hot-loader/babel' ]
        }
      },

      {
        test: /\.scss$/,
        include: [ resolve('src') ],

        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      },

      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: { limit: 8192 }
          }
        ]
      }
    ]
  },

  resolve: {
    modules: [
      resolve('src'),
      'node_modules'
    ],

    extensions: [ '.js', '.jsx', '.json', '.scss' ],

    alias: {
      '@': resolve('src'),
      '@s': resolve('src/screens'),
      '@lib': resolve('src/libs')
    }
  },

  devServer: {
    contentBase: resolve('out'),
    compress: true,
    host: '0.0.0.0',
    port: 3016,
    historyApiFallback: true,
    hotOnly: true,
    https: false,
    noInfo: true,
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Phonebook',
      template: 'templates/index.ejs'
    }),
    new webpack.HotModuleReplacementPlugin()
  ]
}
