import configClient from '@lib/apiClient'

const apiClient = configClient()

// ==================== ACTION TYPES ==================== //
export const SEARCHED_VALUE_CHANGED = 'SEARCHED_VALUE_CHANGED'
export const REQUEST_PHONES = 'REQUEST_PHONES'
export const RECEIVE_PHONES = 'RECEIVE_PHONES'
export const REMOVE_NUMBER = 'REMOVE_PHONE'

// ==================== ACTION CREATORS ==================== //
export const updateSearchedValue = payload => ({ type: SEARCHED_VALUE_CHANGED, payload })

export const receivePhones = payload => ({ type: RECEIVE_PHONES, payload })

export const requestPhones = (payload = { value: '' }) => dispatch => {
  dispatch({ type: REQUEST_PHONES, payload })

  return apiClient
    .phones.get(payload.value)
    .then(data => dispatch(receivePhones(data)))
}

export const removePhone = payload => dispatch => {
  dispatch({ type: REMOVE_NUMBER, payload })

  return apiClient
    .phones.remove(payload.number)
    .then(() => dispatch(requestPhones()))
}
