import { combineReducers } from 'redux'
import initialState from '@/storeShape'

import {
  REQUEST_PHONES,
  SEARCHED_VALUE_CHANGED,
  RECEIVE_PHONES
} from './actions'

const searchResults = (state = initialState.phones.searchResults, action) => {
  const { payload } = action

  switch (action.type) {
    case RECEIVE_PHONES:
      return { ...state, data: payload }
    default:
      return state
  }
}

const query = (state = initialState.phones.query, action) => {
  const { payload } = action

  switch (action.type) {
    case RECEIVE_PHONES:
      return { ...state, isExecuting: false }
    case REQUEST_PHONES:
      return { ...state, ...payload, isExecuting: true }
    case SEARCHED_VALUE_CHANGED:
      return { ...state, value: payload.value }
    default:
      return state
  }
}

const searchPhonesReducer = combineReducers({
  searchResults,
  query
})

export default searchPhonesReducer
