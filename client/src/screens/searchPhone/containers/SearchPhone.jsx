import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'

import translate from '@/i18n'
import { AppLayout } from '@s/root/containers'

import {
  requestPhones,
  updateSearchedValue,
  removePhone
} from '../actions'

const SearchPhoneView = (props) => {
  const {
    t, routes,
    isQueryAllowed, query, searchResults,

    onSearch,
    onSearchedValueChange,
    onRemoveNumber
  } = props

  return (
    <AppLayout t={t} routes={routes}>
      <div>

        <FiltersPanel
          t={t}
          query={query}
          isQueryAllowed={isQueryAllowed}
          onSearch={onSearch}
          onSearchedValueChange={onSearchedValueChange}
        />

        <PhonesGrid
          t={t}
          searchResults={searchResults}
          onRemoveNumber={onRemoveNumber}
        />

      </div>
    </AppLayout>
  )
}

const FiltersPanel = ({ t, query, onSearch, onSearchedValueChange, isQueryAllowed }) => {
  let phoneNuberInput = null

  const handleOnSearch = () => isQueryAllowed && onSearch({
    value: phoneNuberInput.value
  })

  const handleOnClear = () => {
    if (!isQueryAllowed) return

    phoneNuberInput.value = ''
    phoneNuberInput.focus()
    onSearchedValueChange('')
  }

  const handleOnRefresh = () => {
    handleOnClear()
    onSearch({ value: '' })
  }

  return (
    <div className='columns'>

      <div className='column is-6'>
        <div className='field is-grouped'>
          <div className='control is-expanded has-icons-left'>
            <input
              className='input'
              type='tel'
              placeholder={t('searchPhone.inputPlaceholder')}
              disabled={query.isExecuting}
              ref={i => { phoneNuberInput = i }}
              onChange={e => { onSearchedValueChange(e.target.value) }}
            />
            <span className='icon is-small is-left'>
              <i className='fa fa-search' />
            </span>
          </div>

          <div className='control'>
            <a
              className={classNames('button is-info', { 'is-loading': query.isExecuting })}
              onClick={handleOnSearch}
              disabled={!isQueryAllowed}
            >
              {t('searchPhone.searchButtonTitle')}
            </a>
          </div>

          <div className='control'>
            <a
              className={'button'}
              onClick={handleOnClear}
              disabled={!isQueryAllowed}
            >
              {t('searchPhone.clearButtonTitle')}
            </a>
          </div>
        </div>
      </div>

      <div className='column is-offset-5 is-1'>
        <div className='level'>
          <div className='level-left' />
          <div className='level-right'>
            <button className='button' onClick={handleOnRefresh}>
              <span className='icon is-small'>
                <i className='fa fa-refresh' />
              </span>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

const RemoveButton = ({ onClick }) => (
  <span className='icon is-small' onClick={onClick}>
    <i className='fa fa-trash' />
  </span>
)

const PhonesGrid = ({ t, searchResults, onRemoveNumber }) => (
  <div className='columns'>
    <div className='column is-12'>
      <table className='table is-fullwidth is-bordered is-hoverable is-striped'>
        <thead>
          <tr>
            <td>{t('searchPhone.grid.name')}</td>
            <td>{t('searchPhone.grid.number')}</td>
            <td />
          </tr>
        </thead>
        <tbody>
          {searchResults.data.map(row => (
            <tr key={row.number}>
              <td>{row.name}</td>
              <td>{row.number}</td>
              <td>
                <RemoveButton onClick={() => onRemoveNumber(row.id)} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </div>
)

const SearchPhoneScreen = connect(
  ({ phones }) => ({
    ...phones,
    isQueryAllowed: !phones.query.isExecuting && (phones.query.value.trim() !== '')
  }),

  dispatch => ({
    onSearch: query => { dispatch(requestPhones(query)) },
    onSearchedValueChange: value => { dispatch(updateSearchedValue({ value })) },
    onRemoveNumber: number => { dispatch(removePhone({ number })) }
  })
)(SearchPhoneView)

export default translate(SearchPhoneScreen)
