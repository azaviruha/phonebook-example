import configClient from '@lib/apiClient'

const apiClient = configClient()

// ==================== ACTION TYPES ==================== //
export const NEW_PHONE_CHANGED = 'NEW_PHONE_CHANGED'
export const REQUEST_NEW_PHONE_SAVING = 'REQUEST_NEW_PHONE_SAVING'
export const RECEIVE_NEW_PHONE_SAVED = 'RECEIVE_NEW_PHONE_SAVED'
// export const REMOVE_NUMBER = 'REMOVE_PHONE'

// ==================== ACTION CREATORS ==================== //
export const updateNewPhoneData = payload => ({
  type: NEW_PHONE_CHANGED,
  payload
})

export const saveNewPhone = payload => (dispatch, getState) => {
  dispatch({ type: REQUEST_NEW_PHONE_SAVING })

  const { newPhone } = getState()

  return apiClient
    .phones.create(newPhone.data)
    .then(() => dispatch({ type: RECEIVE_NEW_PHONE_SAVED }))
}
