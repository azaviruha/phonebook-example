import initialState from '@/storeShape'

import {
  NEW_PHONE_CHANGED,
  REQUEST_NEW_PHONE_SAVING,
  RECEIVE_NEW_PHONE_SAVED
} from './actions'

const newPhone = (state = initialState.newPhone, action) => {
  const payload = action.payload
  const { data, isSaving } = state

  switch (action.type) {
    case NEW_PHONE_CHANGED:
      return { data: { ...data, ...payload }, isSaving }
    case REQUEST_NEW_PHONE_SAVING:
      return { data: { ...data }, isSaving: true }
    case RECEIVE_NEW_PHONE_SAVED:
      return { data: { ...initialState.newPhone.data }, isSaving: false }
    default:
      return state
  }
}

export default newPhone
