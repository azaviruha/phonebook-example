import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'

import translate from '@/i18n'
import { AppLayout } from '@s/root/containers'
import { updateNewPhoneData, saveNewPhone } from '../actions'

const FormRow = ({ title, children }) => (
  <div className='field'>
    <label className='label'>{title}</label>
    <div className='control is-expanded'>{children}</div>
  </div>
)

const NewPhoneView = (props) => {
  const {
    t, routes,
    data, isSaveAllowed, isCancelAllowed,
    onDataChange, onSavePhone
  } = props

  const handleClearForm = () => {
    onDataChange({ name: '', number: '' })
  }

  return (
    <AppLayout t={t} routes={routes}>
      <div className='container'>
        <div className='columns'>
          <div className='column is-6'>

            <FormRow title='ФИО'>
              <input
                className='input'
                type='text'
                value={data.name}
                onChange={e => onDataChange({ name: e.target.value })}
              />
            </FormRow>

            <FormRow title='Номер'>
              <input
                className='input'
                type='text'
                value={data.number}
                onChange={e => onDataChange({ number: e.target.value })}
              />
            </FormRow>

            <div className='field is-grouped'>
              <div className='control'>
                <a
                  className={classNames('button is-info', { 'is-loading': false })}
                  onClick={() => onSavePhone()}
                  disabled={!isSaveAllowed}
                >
                  {t('newPhone.addButtonTitle')}
                </a>
              </div>

              <div className='control'>
                <a
                  className='button'
                  onClick={handleClearForm}
                  disabled={!isCancelAllowed}
                >
                  {t('newPhone.cancelButtonTitle')}
                </a>
              </div>
            </div>

          </div>
        </div>
      </div>
    </AppLayout>
  )
}

const NewPhoneScreen = connect(
  ({ newPhone }) => ({
    ...newPhone,
    isSaveAllowed: newPhone.data.name.length && newPhone.data.number.length,
    isCancelAllowed: newPhone.data.name.length || newPhone.data.number.length
  }),

  dispatch => ({
    onDataChange: data => { dispatch(updateNewPhoneData(data)) },
    onSavePhone: () => { dispatch(saveNewPhone()) }
  })
)(NewPhoneView)

export default translate(NewPhoneScreen)
