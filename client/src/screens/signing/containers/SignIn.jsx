import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import translate from '@/i18n'
import { FlexContainer, FlexItem } from '@/components'
import { signIn } from '@s/root/actions'

// === Aliases ===
const ScreenContainer = FlexContainer

const ScreenHeader = ({ children }) => (
  <FlexItem><h1>{children}</h1></FlexItem>
)

const ScreenBody = ({ children }) => (
  <FlexItem>
    <FlexContainer direction='topdown'>{children}</FlexContainer>
  </FlexItem>
)

const Field = ({ children }) => (
  <FlexItem>
    <FlexContainer>{children}</FlexContainer>
  </FlexItem>
)

const SignInScreenView = ({ t, onSignIn }) => {
  let emailInput, passwordInput

  const onSignInHandler = () => onSignIn(emailInput.value, passwordInput.value)

  return (
    <div className='hero is-fullheight'>
      <div className='hero-head'></div>

      <div className='hero-body'>
        <div className='container'>

          <div className='columns'>
            <div className='column is-offset-4 is-4'>
              <div className='field'>
                <label className='label'>{t('signIn.emailField')}</label>
                <div className='control has-icons-left has-icons-right'>
                  <input className='input' type='email'  ref={(i) => { emailInput = i }}/>
                  <span className='icon is-small is-left'>
                    <i className='fa fa-envelope'></i>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className='columns'>
            <div className='column is-offset-4 is-4'>
              <div className='field'>
                <label className='label'>{t('signIn.passwordField')}</label>
                <div className='control has-icons-left'>
                  <input className='input' type='text'  ref={(i) => { passwordInput = i }}/>
                  <span className='icon is-small is-left'>
                    <i className='fa fa-lock'></i>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className='columns'>
            <div className='column is-offset-4 is-4'>
              <div className='field'>
                <div className='control'>
                  <button className='button is-primary' onClick={onSignInHandler}>
                    {t('signIn.loginButton')}
                  </button>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div className='hero-foot'></div>
    </div>
  )
}



SignInScreenView.propTypes = {
  t: PropTypes.func.isRequired
}

const SignInScreen = connect(
  null,
  dispatch => ({
    onSignIn: (email, password) => { dispatch(signIn({ email, password })) }
  })

)(SignInScreenView)

export default translate(SignInScreen)
