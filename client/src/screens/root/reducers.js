import { combineReducers } from 'redux'
import access from 'access-levels'

import initialState from '@/storeShape'

import {
  SIGN_IN,
  SIGN_OUT
} from './actions'

import { reducer as searchPhone } from '@s/searchPhone'
import { reducer as newPhone } from '@s/newPhone'

const { anon, user } = access().roles

const processUser = (state = initialState.user, action) => {
  switch (action.type) {
    case SIGN_IN:
      return checkAuth(action.payload) ? { ...state, role: user } : state
    case SIGN_OUT:
      return { ...state, role: anon }
    default:
      return state
  }
}

const rootReducer = combineReducers({
  user: processUser,
  phones: searchPhone,
  newPhone
})

function checkAuth ({ email, password }) {
  return (email === 'test') && (password === '42')
}

export default rootReducer
