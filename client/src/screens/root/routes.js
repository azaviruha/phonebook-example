import React from 'react'
import access from 'access-levels'

import { SearchPhone } from '@s/searchPhone'
import { NewPhone } from '@s/newPhone'
import { SignIn } from '@s/signing'

const { anon, user } = access().accessLevels

const routes = [
  {
    path: '/sign-in',
    isExact: true,
    component: SignIn,
    accessLevel: anon,
    titleKey: 'signIn.screenTitle',
    menutitleKey: 'signIn.screenTitle'
  },

  {
    path: '/',
    isExact: true,
    component: SearchPhone,
    accessLevel: user,
    titleKey: 'searchPhone.screenTitle',
    menuTitleKey: 'searchPhone.menuTitle'
  },

  {
    path: '/new-phone',
    isExact: true,
    component: NewPhone,
    accessLevel: user,
    titleKey: 'newPhone.screenTitle',
    menuTitleKey: 'newPhone.menuTitle'
  }
]

export default routes
