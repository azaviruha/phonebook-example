import React from 'react'
import { Route, Link } from 'react-router-dom'
import classNames from 'classnames'

import translate from '@/i18n'

export const AppLayout = translate(({ children, onSignOut, t, routes }) => (
  <div className='container'>
    <section className='hero is-primary app-header'>
      <div className='hero-head'>
        <header className='navbar'>
          <div className='container'>
            <div className='navbar-brand'>
              <a className='navbar-item'>
                <h1 className='title is-2'>Phonebook</h1>
              </a>
            </div>
            <div id='navbarMenuHeroC' className='navbar-menu'>
              <div className='navbar-end'>
                <span className='navbar-item'>
                  <a className='button is-success is-inverted' onClick={onSignOut}>
                    <span className='icon'>
                      <i className='fa fa-sign-out' />
                    </span>
                    <span>{t('signOutTitle')}</span>
                  </a>
                </span>
              </div>
            </div>
          </div>
        </header>
      </div>
    </section>


    <div className='columns is-2 is-variable'>
      <div className='column is-2 is-warning'>
        <AppMenu t={t} routes={routes} />
      </div>

      <div className='column is-10 is-success'>
        <AppHeader t={t} routes={routes} />
        {React.cloneElement(children, { routes })}
      </div>
    </div>

  </div>
))

export const AppHeader = ({ t, routes = [] }, idx) => (
  routes.map(({ path, isExact, titleKey }, idx) => (
    <Route key={idx} exact path={path} render={() => (
      <h1 className='title is-3'>{t(titleKey)}</h1>
    )}>
    </Route>
  ))
)

export const AppMenu = ({ t, routes = [] }) => (
  <aside className='menu'>
    <ul className='menu-list'>
      {routes.map(({ path, menuTitleKey }, idx) => (
        <MenuItem key={idx} to={path}>{t(menuTitleKey)}</MenuItem>
      ))}
    </ul>
  </aside>
)

export const MenuItem = ({ children, to }) => {
  const MenuItem = ({ match }) => {
    const classes = classNames({ 'is-active': match && match.isExact })

    return (
      <li>
        <Link className={classes} to={to}>{children}</Link>
      </li>
    )
  }

  return (
    <Route path={to} children={MenuItem} />
  )
}
