import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'

import '@/styles/main'
import AuthorizedApp from './AuthorizedApp'
import routes from '../routes'
import configureStore from '../configStore'

const RoutedApp = ({ t }) => (
  <Router>
    <AuthorizedApp routes={routes} lift='/sign-in' />
  </Router>
)

const SelfServicePortal = () => (
  <Provider store={configureStore()}>
    <RoutedApp />
  </Provider>
)

export default SelfServicePortal
