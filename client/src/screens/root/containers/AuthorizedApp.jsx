import React from 'react'
import { connect } from 'react-redux'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'

const AuthorizedApp = ({ routes, lift, user }) => {
  const visibleRoutes = routes.filter(({ accessLevel }) => user.role.bitMask & accessLevel)

  return (
    <Switch>
      {routes.map(({ path, component, accessLevel, ...props }, idx) => {
        const Component = component
        const hasAccess = Boolean(user.role.bitMask & accessLevel)
        const key = `${path}-${idx}`

        const render = () => hasAccess
          ? (<Component routes={visibleRoutes} {...props} />)
          : (<Redirect key={`redirect-${idx}`} from={path} to={lift === path ? '/' : lift} push />)

        return (<Route key={key} exact path={path} render={render} />)
      })}
    </Switch>
  )
}

export default withRouter(connect(({ user }) => ({ user }))(AuthorizedApp))
