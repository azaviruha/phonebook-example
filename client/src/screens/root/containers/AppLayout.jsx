import React from 'react'
import { connect } from 'react-redux'

import { AppLayout as AppLayoutView } from '@/components'
import { signOut } from '@s/root/actions'


const AppLayout = connect(
  null,

  dispatch => ({
    onSignOut: () => { dispatch(signOut()) }
  })
)(AppLayoutView)

export default AppLayout