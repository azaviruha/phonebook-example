import { requestPhones } from '@s/searchPhone/actions'

export const SIGN_OUT = 'SIGN_OUT'
export const SIGN_IN = 'SIGN_IN'
export const INIT_APP = 'INIT_APP'

export const signOut = () => ({ type: SIGN_OUT })

export const signIn = payload => ({ type: SIGN_IN, payload })

export const initApp = () => dispatch => {
  dispatch(requestPhones())
}
