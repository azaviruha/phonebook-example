import fetch from 'isomorphic-fetch'

const defaultConfig = {
  host: 'http://127.0.0.1',
  port: 3017
}

const cors = { mode: 'cors' }

export default function (config = defaultConfig) {
  const { host, port } = config
  const urlBase = `${host}:${port}`

  const phones = {
    get (numberPart = '') {
      return fetch(`${urlBase}/phones?number_like=${numberPart}`, {
        ...cors,
        method: 'GET',
        headers: {
          'Accept': 'application/json'
        }
      })
        .then(resp => resp.json())
    },

    remove (id) {
      return fetch(`${urlBase}/phones/${id}`, {
        ...cors,
        method: 'DELETE'
      })
    },

    create (phone) {
      return fetch(`${urlBase}/phones`, {
        ...cors,
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(phone)
      })
        .then(resp => resp.json())
    }
  }

  return {
    phones
  }
}
