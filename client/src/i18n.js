import i18n from 'i18next'
import { translate } from 'react-i18next'

i18n
  .init({
    resources: {
      ru: {
        translations: {
          signOutTitle: 'Выйти',

          signIn: {
            screenTitle: 'Добро пожаловать',
            emailField: 'E-mail',
            passwordField: 'Пароль',
            loginButton: 'Войти'
          },

          searchPhone: {
            screenTitle: 'Поиск номера телефона',
            menuTitle: 'Найти',
            searchButtonTitle: 'Искать',
            clearButtonTitle: 'Очистить',
            inputPlaceholder: 'Номер телефона',
            grid: {
              number: 'Номер телефона',
              name: 'Абонент'
            }
          },

          newPhone: {
            screenTitle: 'Добавление номера телефона',
            menuTitle: 'Добавить',
            addButtonTitle: 'Добавить',
            cancelButtonTitle: 'Отмена'
          }
        }
      }
    },

    fallbackLng: 'ru',

    ns: ['translations'],
    defaultNS: 'translations',

    interpolation: {
      escapeValue: false,
      formatSeparator: ','
    },

    react: {
      wait: true
    }

  })

translate.setI18n(i18n)

export default translate()
